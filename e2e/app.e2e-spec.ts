import { ICTreesPage } from './app.po';

describe('ictrees App', function() {
  let page: ICTreesPage;

  beforeEach(() => {
    page = new ICTreesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
