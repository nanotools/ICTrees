import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { D3TreeRendererComponent } from './d3-tree-renderer/d3-tree-renderer.component';

@NgModule({
    imports: [ SharedModule ],
    exports: [ SharedModule, D3TreeRendererComponent ],
    declarations:  [ D3TreeRendererComponent ],
})
export class TreeRenderersModule { }
