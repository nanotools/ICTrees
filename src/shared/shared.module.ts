import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'shared/modal/modal.component';

@NgModule({
    imports: [ CommonModule, NgbModule, FormsModule ],
    exports: [ CommonModule, FormsModule, NgbModule, ModalComponent ],
    declarations:  [ ModalComponent ],
})
export class SharedModule { }
