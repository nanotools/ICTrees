import { Component, OnInit, Input, Output } from '@angular/core';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'lodash';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  public data = {};
  public _ = _;
  @Input() title: string;
  @Input() submitText: string;
  @Input() items: {var: string, type: string, label: string, value: any, placeholder: string}[];
  @Input() close;
  @Input() dismiss;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    for (let item of this.items) {
      this.data[item.var] = item.value;
    }
  }

  evalCond(condDesc: {dataAttribute: string, dataValue: string}) {
    if (!condDesc) {
      return true;
    }
    return this.data[condDesc.dataAttribute] === condDesc.dataValue;
  }

}
