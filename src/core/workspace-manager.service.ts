import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { WorkspaceModel } from '../models/workspace.model';

@Injectable()
export class WorkspaceManagerService {
    public workspaces: WorkspaceModel[];
}