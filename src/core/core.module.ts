import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { D3CreatorService } from './d3-creator.service';
import { WorkspaceManagerService } from './workspace-manager.service';

@NgModule({
  imports: [
    CommonModule, // we use ngFor
  ],
  providers: [
    D3CreatorService,
    WorkspaceManagerService
  ]
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
