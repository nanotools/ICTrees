import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationStart, GuardsCheckStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ICTrees';
  isCollapsed = true;
  sentence = '<Pron>I,<V>see,<N>trees,<P>of,<A>green';
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe((ev: any) => {
      console.log(ev);
      let url = '';
      if (ev instanceof NavigationStart) {
        url = ev.url;
      } else if (ev instanceof GuardsCheckStart) {
        url = ev.urlAfterRedirects.toString();
      }
      if (url.startsWith('/tree')) {
        console.log('TREE PAGE');
        let params = decodeURIComponent(url.substr(5));
        console.log(params);
        if (params.startsWith('/')) {
          this.sentence = params.substr(1);
        } else {
          this.router.navigate(['tree', this.sentence])
        }
      }
    });
  }
}
