import { RouterModule, Routes } from '@angular/router';
import { TreePageComponent } from '../pages/tree/tree.component';
import { HowtoPageComponent } from '../pages/howto/howto.component';
import { AboutPageComponent } from '../pages/about/about.component';

export const appRoutes: Routes = [
    {
      path: '',
      redirectTo: 'tree',
      pathMatch: 'full'
    },
    {
      path: 'tree',
      component: TreePageComponent
    },
    {
      path: 'tree/:constituents',
      component: TreePageComponent
    },
    {
        path: 'howto',
        component: HowtoPageComponent
    },
    {
      path: 'about',
      component: AboutPageComponent
    },
    { path: '',
      redirectTo: 'tree',
      pathMatch: 'full'
    }
  ];
