import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { CoreModule } from '../core/core.module';
import { TreeRenderersModule } from '../tree-renderers/tree-renderers.module';
import { appRoutes } from './app.router';
import { TreePageComponent } from '../pages/tree/tree.component';
import { HowtoPageComponent } from '../pages/howto/howto.component';
import { AboutPageComponent } from '../pages/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    TreePageComponent,
    HowtoPageComponent,
    AboutPageComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    CoreModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true, useHash: true } // <-- debugging purposes only
    ),
    TreeRenderersModule
  ],
  providers: [],
  entryComponents: [TreePageComponent, HowtoPageComponent, AboutPageComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
