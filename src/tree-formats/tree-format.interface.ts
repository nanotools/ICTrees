import { TreeModel } from '../models/tree.model';

interface TreeFormat {
    parse(input: string): TreeModel;
    generate(input: TreeModel): string;
}
