import * as _ from 'lodash';

export class TreeModel {

  parent: TreeModel | null;
  root: string | null;
  value: string | null;
  representation: any = {};
  left: TreeModel | null = null;
  right: TreeModel | null = null;
  trace: TreeModel | null = null;
  traceOf: TreeModel | null = null;

  static merge(root: string, right: TreeModel, left: TreeModel | null = null): TreeModel {
    let mergedTree = new TreeModel(root);
    mergedTree.right = right;
    mergedTree.left = left;
    if (right.parent) {
      if (right.parent.left === right) {
        right.parent.left = mergedTree;
      } else {
        right.parent.right = mergedTree;
      }
      mergedTree.parent = right.parent;
    }
    left.parent = mergedTree;
    right.parent = mergedTree;
    return mergedTree;
  }

  static copy(original: TreeModel): TreeModel {
    let copy = _.cloneDeep(original);
    copy.trace = original;
    original.traceOf = copy;
    return copy;
  }

  static move(root: string, tree: TreeModel, original: TreeModel, trace: string | null = null) {
    let copy = TreeModel.copy(original);
    original.root = trace == null ? null : trace;
    return TreeModel.merge(root, tree, copy);
  }

  getRoot() {
    return !this.parent ? this : this.parent.getRoot();
  }

  containsTrace() {
    let leftTrace = this.left ? this.left.containsTrace() : false;
    let rightTrace = this.right ? this.right.containsTrace() : false;
    return !!this.traceOf || leftTrace || rightTrace;
  }

  constructor(root: string, value?: string) {
    this.root = root;
    this.value = value;
  }

}
