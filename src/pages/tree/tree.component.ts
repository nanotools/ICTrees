import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'lodash';

import { TreeModel } from '../../models/tree.model';
import { D3TreeRendererComponent } from '../../tree-renderers/d3-tree-renderer/d3-tree-renderer.component';

@Component({
  selector: 'tree-page',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreePageComponent implements OnInit {
  private selected: { [key: string]: TreeModel } = { 'first': null, 'second': null };
  public alerts: any[] = [];
  public constituents;
  public windowWidth: number;
  @ViewChild('mergeForm') mergeForm;
  @ViewChild('mergeInForm') mergeInForm;
  @ViewChild('moveForm') moveForm;
  public mergeInFormOptions = [{
    'type': 'text',
    'label': 'HEAD',
    'var': 'mergedHead',
    'value': 'N',
    'placeholder': 'Enter a head to merge'
  },
  {
    'type': 'text',
    'label': 'VALUE',
    'var': 'value',
    'value': 'tree',
    'placeholder': 'Enter a value for a head (optional)'
  },
  {
    'type': 'text',
    'label': 'LABEL',
    'var': 'label',
    'value': 'NP',
    'placeholder': 'Enter a label of merged constituent'
  }];
  public mergeFormOptions = [{
    'type': 'text',
    'label': 'LABEL',
    'var': 'label',
    'value': 'XP',
    'placeholder': 'Enter a label of merged constituent'
  }];
  public moveFormOptions = [{
    'type': 'text',
    'label': 'LABEL',
    'var': 'label',
    'value': 'XP',
    'placeholder': 'Enter a label of merged constituent'
  },
  {
    'type': 'select',
    'label': 'MOVEMENT TYPE',
    'options': {
      'keep': 'Keep original constituent',
      'trace': 'Leave a trace',
      'delete': 'Delete original constituent'
    },
    'var': 'movementType',
    'value': 'keep',
    'placeholder': 'Select the movement type'
  },
  {
    'type': 'text',
    'label': 'TRACE',
    'condition': {
      'dataAttribute': 'movementType',
      'dataValue': 'trace'
    },
    'var': 'trace',
    'value': 't',
    'placeholder': 'Enter a label of moved constituent'
  }
  ];
  @ViewChild(D3TreeRendererComponent) renderer;
  constructor(
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute
  ) {
    this.windowWidth = window.innerWidth - 100;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.constituents = params['constituents'];
    });
  }

  canMergeIn() {
    let allSelected = _.filter(this.selected, (s) => s != null);
    if (allSelected.length !== 1) {
      return false;
    }
    return !allSelected[0].parent;
  }

  canMerge() {
    let allSelected = _.filter(this.selected, (s) => s != null);
    if (allSelected.length !== 2) {
      return false;
    }
    return !allSelected[0].parent && !allSelected[1].parent;
  }

  canMove() {
    let allSelected = _.filter(this.selected, (s) => s != null);
    if (allSelected.length !== 2) {
      return false;
    }
    return !!allSelected[0].parent && !allSelected[0].containsTrace();
  }


  clearSelected() {
    if (this.selected['first'] !== null) {
      this.renderer.onNodeSelected(this.selected['first'], 'unselect');
      this.selected['first'] = null;
    }
    if (this.selected['second'] !== null) {
      this.renderer.onNodeSelected(this.selected['second'], 'unselect');
      this.selected['second'] = null;
    }
  }

  canUnselect() {
    return this.selected['first'] !== null || this.selected['second'] !== null;
  }

  onNodeSelected(node: TreeModel) {
    if (this.selected['first'] === node) {
      this.renderer.onNodeSelected(node, 'unselect');
      this.selected['first'] = null;
    } else if (this.selected['second'] === node) {
      this.renderer.onNodeSelected(node, 'unselect');
      this.selected['second'] = null;
    } else if (this.selected['first'] === null) {
      this.renderer.onNodeSelected(node, 'selectFirst');
      this.selected['first'] = node;
    } else if (this.selected['second'] === null) {
      this.renderer.onNodeSelected(node, 'selectSecond');
      this.selected['second'] = node;
    } else {
      this.alerts.push({
        message: 'YOU CAN\'T SELECT MORE THAN TWO NODES',
        type: 'danger'
      });
    }
  }

  mergeNewNode(mergeInForm) {
    let allSelected = _.filter(this.selected, (s) => s != null);
    this.modalService.open(mergeInForm).result.then((result) => {
        this.renderer.mergeIn(result['mergedHead'], result['label'], allSelected[0], result['value'] !== '' ? result['value'] : null);
        this.cleanSelection();
    }, (reason) => {
      console.log(reason);
    });
  }

  clear() {
    this.cleanSelection();
    this.renderer.clear();
  }

  public closeAlert(alert) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }

  @HostListener('window:keydown', ['$event'])
  shortcut($event) {
    if ($event.ctrlKey && $event.altKey && $event.key === 'm') {
      this.move(this.moveForm);
    } else if ($event.ctrlKey && $event.key === 'm') {
      this.mergeNodes(this.mergeForm);
    } else if ($event.ctrlKey && $event.key === 'i') {
      this.mergeNewNode(this.mergeInForm);
    }
  }

  mergeNodes(mergeForm) {
    let allSelected = _.filter(this.selected, (s) => s != null);
    this.modalService.open(mergeForm).result.then((result) => {
        this.renderer.merge(result['label'], allSelected[0], _.last(allSelected) );
        this.cleanSelection();
    }, (reason) => {
      console.log(reason);
    });
  }

  cleanSelection() {
    for (let id in this.selected) {
      if (this.selected[id] != null) {
        this.renderer.onNodeSelected(this.selected[id], 'unselect');
        this.selected[id] = null;
      }
    }
  }

  move(moveForm) {
    let allSelected = _.filter(this.selected, (s) => s != null);
    this.modalService.open(moveForm).result.then((result) => {
      let trace = result['trace'];
      if (result['movementType'] === 'keep') {
        trace = 'keep';
      } else if (result['movementType'] === 'delete') {
        trace = null;
      }
      this.renderer.move(result['label'], allSelected[1], allSelected[0], trace );
      this.cleanSelection();
    }, (reason) => {
      console.log(reason);
    });
  }

}
